import React from 'react';
import './Tasks.css';
import Task from './Task';

const Tasks = props => {
    return (
        <div className='tasks'>
            {props.tasks.map(task => <Task text={task.text}
                                           key={task.id}
                                           remove={() => props.removeTask(task.id)}
                                           done={task.checkbox}
                                            checkbox={() => props.checkBox(task.id)}
                />
            )}
        </div>
    );
};

export default Tasks;