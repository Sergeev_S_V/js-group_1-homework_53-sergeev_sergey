import React from 'react';
import './Task.css';

const Task = (props) => {
    return (
        <div className='Task'>
            <p>{props.text}</p>
            <span onClick={props.remove}>x</span>
            <div><button onClick={props.checkbox}>Task complete</button></div>
            {props.done ? <p>Задание выполнено</p> : <p>Не выполнено</p>}
        </div>
    );
};

export default Task;