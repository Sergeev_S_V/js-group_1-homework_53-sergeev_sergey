import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './AddTaskForm';
import Tasks from './Tasks';

class App extends Component {
  state = {
    tasks: [
        {text: 'Hello! My name is Jack', id: '0', checkbox: false},
        {text: 'Hello Jack! My name is Sofia', id: '1', checkbox: false},
        {text: 'Hello everything! I am Donald Trump', id: '2', checkbox: false}
    ],
    currentText: ''
  };

  nextId = 3;

  currentText = (event) => {
    let currentText = event.target.value;

    this.setState({currentText});
  };

  addNewTask = () => {
    const tasks = [...this.state.tasks];
    if (this.state.currentText.length > 0) {
      const newTask = {
          text: this.state.currentText,
          id: this.nextId,
          checkbox: false
      };
      tasks.unshift(newTask);
      this.nextId++;
    }

    this.setState({tasks});
  };

  removeTask = (id) => {
    const index = this.state.tasks.findIndex(task => task.id === id);
    const tasks = [...this.state.tasks];
    tasks.splice(index, 1);

    this.setState({tasks});
  };

  checkBox = (id) => {
    const index = this.state.tasks.findIndex(task => task.id === id);
    const task = {...this.state.tasks[index]};
    task.checkbox = !task.checkbox;

    const tasks = [...this.state.tasks];
    tasks[index] = task;

    this.setState({tasks});
  };

  render() {
    return (
      <div className="App">
          <div className='Container'>
              <AddTaskForm
                  add={this.addNewTask}
                  text={this.currentText}
              />
              <Tasks
                  tasks={this.state.tasks}
                  removeTask={this.removeTask}
                  checkBox={this.checkBox}
              />
          </div>
      </div>
    );
  }
}

export default App;
